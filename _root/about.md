![]({{urls.media}}/me.png) I've been working in the software industry since I
graduated as a Computer Scientist in 2003 in the [University of a
Coruña](http://www.udc.es/.) I've been also cooperating with the MADS research
group, either in shared industry projects or as part of personal research.

I started writing software for digital video streaming, mostly in C, but
gradually moved to erlang, which is my favourite language today. Anyway, I
believe languages are to software as instruments to music; it is hard to compose
a good symphony with a funnel, but you can do something decent (or at least
funny :) if you are a skilled musician. If you can't play music, you can't play
it, it doesn't matter you use a 10K^N $ Stradivarius. So, I like to play erlang,
but what I like most is to play software.

 * **Public Key:** 0x53C40770
 * E-mail: samuelrivas [at] gmail [dot] com
 * Twitter: @samuel_rivas
 * Linked-in: samuelrivas