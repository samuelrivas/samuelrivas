I had the pleasure to speak in these events:

 * Erlang User Conference 2014: [***Ask not what your Erlang can do for
   you***](http://www.erlang-factory.com/euc2014/samuel-rivas)
 * Erlang User Conference 2010: [***Testing what should work, not what should
   not fail***]
   (http://www.erlang-factory.com/conference/ErlangUserConference2010/speakers/SamuelRivas)
 * International Workshop on Automation of Software Test 2010: ***Property
   Driven Development In Erlang, By Example***
 * Erlang Factory 2009: [***Developing a Set Top Box Middleware in Erlang***]
   (http://www.erlang-factory.com/conference/London2009/speakers/samuelrivas)
 * Erlang User Conference 2007: [***Erlang Developments in
   LambdaStream***](http://www.erlang.se/euc/07/)
 * Multimedia Services Access Networks 2005: ***A Versatile Multiplexing
   Algorithm Exclusively Based On The MPEG-2 Systems Layer***

And I'm also proud of co-authoring some articles that have been published:

 * Pablo Montero, Víctor M. Gulías, Javier Taibo, and Samuel
   Rivas. ***Optimising Lossless Stages in a GPU-based MPEG Encoder*** [Springer
   Multimedia Tools and
   Applications](http://link.springer.com/article/10.1007%2Fs11042-012-1053-9)
   Vol. 65, Issue 3, pp 495-520. 7 August, 2013
 * Agneta Nilsson, Laura M. Castro, Samuel Rivas, and Thomas Arts. ***Assessing
   the effects of introducing a new software development process: a
   methodological description*** [International Journal on Software Tools for
   Technology Transfer.](http://link.springer.com/journal/10009) April, 2013
 * Javier Taibo, Víctor M. Gulías, Pablo Montero, and Samuel Rivas. ***GPU-based
   Fast Motion Estimation for On-the-Fly Encoding of Computer-Generated Video
   Streams*** [International Workshop on Network and Operating Systems Support
   for Digital Audio and Video (NOSSDAV
   2011).](http://nss.cs.ubc.ca/nossdav2011/) June, 2011
   [[Article]](http://portal.acm.org/citation.cfm?id=1989260)
 * Pablo Montero, Javier Taibo, Víctor M. Gulías, and Samuel Rivas. ***Parallel
   Zigzag Scanning and Huffman Coding for a GPU-based MPEG-2 Encoder*** [2010
   IEEE International Symposium on Multimedia (ISM
   2010).](http://ism2010.asia.edu.tw/) 13-15 December, 2010
 * David Castro, Clara Benac Earle, Lars-Åke Fredlund, Víctor M. Gulías, and
   Samuel Rivas. ***Using McErlang to Verify an Erlang Process Supervision
   Component*** [Trends in Functional Programming
   2010.](http://www.cs.ou.edu/tfp2010/) 17-19 May, 2010
   [[Article]](http://madsgroup.org/staff/samuel/files/articles/tfp10.pdf)
 * Samuel Rivas, Miguel Ángel Francisco, and Víctor M. Gulías. ***Property
   Driven Development in Erlang, by Example*** [International Workshop on
   Automation of Software Test.](http://www.cs.allegheny.edu/ast2010) 3-4 May,
   2010 [[Article]](http://madsgroup.org/staff/samuel/files/articles/ast10.pdf)
   [[Slides]](http://madsgroup.org/staff/samuel/files/articles/ast10-slides.pdf)
   [[Slides
   Source]](http://madsgroup.org/staff/samuel/files/articles/ast10-slides.tar.bz2)
 * Samuel Rivas, Miguel Barreiro, and Víctor M. Gulías. ***Interactive Playout
   of Digital Video Streams*** [Encyclopedia of Multimedia Technology and
   Networking (2nd
   edition)](http://www.igi-global.com/Bookstore/Chapter.aspx?TitleId=17475)
   2009
 * Samuel Rivas, Miguel Barreiro, and Víctor M. Gulías. ***Multiplexing Digital
   Multimedia Data*** [Encyclopedia of Multimedia Technology and Networking (2nd
   edition)](http://www.igi-global.com/Bookstore/Chapter.aspx?TitleId=17512)
   2009
 * Samuel Rivas, Carlos Abalde, and Víctor M. Gulías. ***A Versatile
   Multiplexing Algorithm Exclusively Based on the MPEG-2 Systems Layer***
   [International Conference on Multimedia Services Access
   Networks (MSAN'05)](http://ieeexplore.ieee.org/xpl/RecentCon.jsp?punumber=9967)
   13-15 June,
   2005 [[Article]](http://madsgroup.org/staff/samuel/files/articles/msan05.pdf)
   [[Slides]](http://madsgroup.org/staff/samuel/files/articles/msan05-slides.pdf)
   [[Slides
   Source]](http://madsgroup.org/staff/samuel/files/articles/msan05-slides.tar.bz2)
